#ifndef CAMERA_H
#define CAMERA_H

#include "../include/geometry.h"
#include "../include/pixel.h"
#include "../include/quadtree.h"


typedef struct{
    Vect3D pos;
    float anglevue;
    Node* champvision;
    int nbNodechamp;
    void initCamera(Vect3D pos,float angle){
        this->pos=pos;
        this->anglevue=angle;
        this->nbNodechamp=0;
        champvision=(Node*) malloc(200000*sizeof(Node));
    }
}Camera;


Vect3D direction (float theta,float phi);
Vect3D pointvise (Vect3D position,float theta,float phi);
Vect3D left(float theta);
Vect3D up(float theta,float phi);
Vect3D plusproche(Node n, Vect3D poscam);



#endif