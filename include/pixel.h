#ifndef PIXEL_H
#define PIXEL_H

#include <iostream>

using namespace std;

typedef struct{
    float x;
    float y;
    float valeur;//couleur
    float altitude(float zmin,float zmax){
        return zmin+this->valeur/255*(zmax-zmin);     
    }
    void affichePixel(){
        cout<<" x="<< this->x<< " y="<<this->y<<endl;
    }
} Pixel;

Pixel position(float x,float y);
Pixel divise (Pixel pixel,float d);

#endif