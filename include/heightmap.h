#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include "../include/pixel.h"

typedef struct Image {
	int w,h;
	Pixel* dat;
} Image;

Image* Charger(const char* fichier);
Image* NouvelleImage(int w,int h);

#endif