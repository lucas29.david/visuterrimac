#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include "../include/quadtree.h"
#include "../include/frustumculling.h"

void SDLaffiche(FrustumCulling* frust,QuadTree* q,float zmin,float zmax);
//void SDLtriangle( Pixel p1,Pixel p2,Pixel p3);
void SDLNode (Node* n,float zmin,float zmax);
void SDLNodevide (Node* n,float zmin,float zmax);

void affiche (Node* n,float zmin,float zmax);

void affichevide (Node* n,float zmin,float zmax);

#endif