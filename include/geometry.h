#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cmath>
#include "../include/pixel.h"

typedef struct{
    float x;
    float y;
    float z;
}Vect3D;


Vect3D createVect3D(float x,float y, float z);
Vect3D produitvectoriel(Vect3D v1,Vect3D v2);
float Thales(float a,float b, float c);
float distance(Vect3D v1,Vect3D v2);


#endif
