#ifndef FRUSTUMCULLING_H
#define FRUSTUMCULLING_H

#include "../include/geometry.h"
#include "../include/quadtree.h"
#include "../include/camera.h"

Vect3D pointfrustum(float angle, float adj);

typedef struct {
    Vect3D fard;
    Vect3D farg;
    Vect3D neard;
    Vect3D nearg;
    Camera cam;
    void init(float zfar,float znear, Vect3D poscam, float fovx){
        fard= pointfrustum(fovx/2,zfar);
        farg=pointfrustum(-fovx/2,zfar);
        neard=pointfrustum(fovx/2,znear);
        nearg=pointfrustum(-fovx/2,znear);
        Camera* caminter;
        caminter=(Camera*) malloc(sizeof(Camera));
        caminter->initCamera(poscam,fovx);
        cam=*caminter;
    }
}FrustumCulling;


bool appartientNode(FrustumCulling* frust,Node* n);
void champvision(FrustumCulling* frust, QuadTree* quadtree);

#endif