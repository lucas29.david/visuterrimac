#ifndef QUADTREE_H
#define QUADTREE_H

#include "../include/pixel.h"
#include <iostream>

using namespace std;

struct Node{//rajout tableau triangle
    Pixel phg;//pixel haut gauche
    Pixel phd;
    Pixel pbg;
    Pixel pbd;//pixel bas droit
    Node* e1;//pointeur sur enfant 1
    Node* e2;
    Node* e3;
    Node* e4;
    void createNode(Pixel p1,Pixel p2,Pixel p3,Pixel p4){
        this->phg=p1;
        this->phd=p2;
        this->pbg=p3;
        this->pbd=p4;
        this->e1=nullptr;
        this->e2=nullptr;
        this->e3=nullptr;
        this->e4=nullptr;
    }
    bool isLeaf(){
        if (this->e1==nullptr and this->e2==nullptr and this->e3==nullptr and this->e4==nullptr){
            return true;
        }
        else
            return false;
    }
    int countNode (){
        if (this->isLeaf())
            return 1;
        else {
            return 1+this->e1->countNode()+this->e2->countNode()+this->e3->countNode()+this->e4->countNode();
        }
    }
    void enfant(Pixel* dat, int longueur, int largeur, int debutLongueur, int debutLargeur){
        //rempli les 4 enfants
        // on créé les pixels necéssaires pour remplir les enfants
        // on alloue de la mémoire
        // on leur donne la bonne position
        // on les implémente avec la valeur correspondente 
        Pixel* centre;
        Pixel* milieudroit;
        Pixel* milieugauche;
        centre = (Pixel*) malloc(sizeof(Pixel));
        milieudroit = (Pixel*) malloc(sizeof(Pixel));
        milieugauche = (Pixel*) malloc(sizeof(Pixel));
        *centre=position((this->phg.x+this->phd.x)/2,(this->pbd.y+this->phd.y)/2);
        *milieudroit=position(this->phd.x,(this->phd.y+this->pbd.y)/2);
        *milieugauche=position(this->phg.x,(this->phg.y+this->pbg.y)/2);
        longueur=longueur-debutLongueur;
        largeur=largeur-debutLargeur;
        if(longueur%2==0){
            milieudroit->valeur=(dat[(largeur*(longueur/2)-1)+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(longueur/2+1)*largeur-1+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur)/2;
            milieugauche->valeur=(dat[longueur/2*largeur+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(longueur*(largeur-1)/2)+debutLargeur+debutLongueur+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur)/2;
            centre->valeur=(dat[(longueur/2-1)*largeur+largeur/2-1+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(longueur/2-1)*largeur+largeur/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(longueur/2)*largeur+largeur/2-1+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(longueur/2)*largeur+largeur/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur)/4;
        }
        else{
            milieudroit->valeur=dat[(largeur*(longueur+1)-2)/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur;
            milieugauche->valeur=dat[longueur/2*largeur+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur;
            centre->valeur=dat[((largeur*(longueur+1)-2)/2+(longueur/2*largeur))/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur;
        }

        //on incrémente les enfants (sens droite gauche haut bas)

        if (abs(this->phd.x-this->phg.x)>1){
            Pixel* milieuhaut;
            milieuhaut = (Pixel*) malloc(sizeof(Pixel));
            if(largeur%2==0){
                milieuhaut->valeur=(dat[(largeur-1)/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(largeur-1)/2+1+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur)/2;
            }
            else{
                milieuhaut->valeur=dat[(largeur-1)/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur;
            }
            this->e1=new Node();
            this->e2=new Node();
            *milieuhaut=position((this->phg.x+this->phd.x)/2,this->phg.y);
            this->e1->createNode(this->phg,*milieuhaut,*milieugauche,*centre);
            this->e2->createNode(*milieuhaut,this->phd,*centre,*milieudroit);
            this->e1->enfant(dat, longueur/2, largeur/2, 0, 0);
            this->e2->enfant(dat, longueur/2, largeur, 0, largeur/2);
        }
        if (abs(this->phd.y-this->pbd.y)>1){
            Pixel* milieubas;
            milieubas = (Pixel*) malloc(sizeof(Pixel));
            if(largeur%2==0){
                milieubas->valeur=(dat[(longueur-1)*largeur+largeur/2-1+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur+dat[(longueur-1)*largeur+largeur/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur)/2;
            }
            else{
                milieubas->valeur=dat[(longueur-1)*largeur+largeur/2+debutLargeur+debutLongueur*(longueur+debutLongueur)].valeur;
            }
            this->e3=new Node();
            this->e4=new Node();
            *milieubas=position((this->pbg.x+this->pbd.x)/2,this->pbg.y);
            this->e3->createNode(*milieugauche,*centre,this->pbg,*milieubas);
            this->e4->createNode(*centre,*milieudroit,*milieubas,this->pbd);
            this->e3->enfant(dat, longueur, largeur/2, longueur/2, 0);
            this->e4->enfant(dat, longueur/2, largeur/2, longueur/2, largeur/2);
        }
    }
};

typedef struct{
    Node* n;
    int profondeur;
    void initQuadTree(Node* n)//,int p)
    {
        this->n=new Node();
        this->n=n;
        //this->profondeur=p; 
    }
}QuadTree;

Node createNode(Pixel p1,Pixel p2,Pixel p3,Pixel p4);
QuadTree initQuadTree(Node* n);//,int p);
void afficheNode(Node* n);
void afficheQuadTree (Node* n);
void afficheQuadTreerecursive(Node* n,int i);

#endif