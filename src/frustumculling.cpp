#include "../include/geometry.h"
#include "../include/camera.h"
#include "../include/frustumculling.h"
#include <math.h>


//Frustum Culling dans le repère de la caméra



Vect3D pointfrustum(float angle, float adj){
        Vect3D point;
        point.x=tan(angle)*adj;
        point.y=adj;
        point.z=0;
        return point;
}

//fonction qui permet de savoir si il y a intersection entre le noeud et le trapèze 
bool appartientNode(FrustumCulling* frust,Node* n){
    if(((n->phd.x<0) and (n->phd.y<frust->farg.y) and (n->phd.x>Thales(frust->nearg.y,n->phd.y*tan(-frust->cam.anglevue/2),frust->nearg.x)) and (n->phd.y>frust->nearg.y))
    or ((n->phg.x>0) and (n->phg.y<frust->fard.y) and (n->phg.x<Thales(frust->neard.y,n->phg.y*tan(frust->cam.anglevue/2),frust->neard.x)) and (n->phg.y>frust->neard.y))
    or ((n->pbd.x<0) and (n->pbd.y<frust->farg.y) and (n->pbd.x>Thales(frust->nearg.y,n->pbd.y*tan(-frust->cam.anglevue/2),frust->nearg.x)) and (n->pbd.y>frust->nearg.y))
    or ((n->pbg.x>0) and (n->phg.y<frust->fard.y) and (n->pbg.x<Thales(frust->neard.y,n->pbg.y*tan(frust->cam.anglevue/2),frust->neard.x)) and (n->pbg.y>frust->neard.y))){
        return true;
    }
    else 
        return false;
}


//fonction qui permet de savoir quelle partie de l'arbre il faut afficher
void champvision(FrustumCulling* frust, QuadTree* quadtree){
    // Malheureusement à l'appel de la fonction affichage on a un défaut de segmentation
    // mais on ne rentre jamais dans la fonction car les cout ne s'affiche pas dans le terminal
    cout<<"frustum";
    if (quadtree->n->isLeaf()){
        if (appartientNode(frust,quadtree->n)){
            cout<<"frustum1";
            frust->cam.champvision[frust->cam.nbNodechamp]=*quadtree->n;
            frust->cam.nbNodechamp++;
            cout<<"frustum2";
        }
    }
    else
    {
        if (quadtree->n->e1!=nullptr){
            cout<<"frustum3";
            QuadTree* q1;
            q1->initQuadTree(quadtree->n->e1);
            champvision(frust,q1);
            cout<<"frustum4";
        }
        if (quadtree->n->e2!=nullptr){
            QuadTree* q2;
            q2->initQuadTree(quadtree->n->e2);
            champvision(frust,q2);
        }
        if (quadtree->n->e3!=nullptr){
            QuadTree* q3;
            q3->initQuadTree(quadtree->n->e3);
            champvision(frust,q3);
        }
        if (quadtree->n->e4!=nullptr){
            QuadTree* q4;
            q4->initQuadTree(quadtree->n->e4);
            champvision(frust,q4);
        }
    }
}