#include <SDL2/SDL.h>
#include <GL/gl.h>

#include "../include/affichage.h"

/* Fonctionne mal
   
void SDLtriangle( Pixel p1,Pixel p2,Pixel p3){
    float zmin=-0.5;
    float zmax=1;
    float d=500;
    Pixel p1d=divise(p1,d);
    Pixel p2d=divise(p2,d);
    Pixel p3d=divise(p3,d);
    glBegin(GL_LINES);
        glVertex3d(p1d.x,p1d.y,p1.altitude(zmin,zmax)/d);
        glVertex3d(p2d.x,p2d.y,p2.altitude(zmin,zmax)/d);
        glVertex3d(p3d.x,p3d.y,p3.altitude(zmin,zmax)/d);
    glEnd();
}

void SDLNode (Node* n){
    SDLtriangle(n->phd,n->phg,n->pbg);
    SDLtriangle(n->phd,n->pbd,n->pbg);

}*/


void SDLNode (Node* n,float zmin,float zmax){
    // affiche le noeud avec deux triangles plein
    
    float d=500;
    zmin=zmin/d;
    zmax=zmax/d;
    
    Pixel phd=divise(n->phd,d);
    Pixel phg=divise(n->phg,d);
    Pixel pbd=divise(n->pbd,d);
    Pixel pbg=divise(n->pbg,d);

    glBegin(GL_TRIANGLE_STRIP);

        glVertex3d(phd.x,phd.y,n->phd.altitude(zmin,zmax)/d);
        glVertex3d(phg.x,phg.y,n->phg.altitude(zmin,zmax)/d);
        glVertex3d(pbg.x,pbg.y,n->pbg.altitude(zmin,zmax)/d);

        glVertex3d(phd.x,phd.y,n->phd.altitude(zmin,zmax)/d);
        glVertex3d(pbd.x,pbd.y,n->pbd.altitude(zmin,zmax)/d);
        glVertex3d(pbg.x,pbg.y,n->pbg.altitude(zmin,zmax)/d);


    glEnd();
}

void SDLNodevide (Node* n,float zmin,float zmax){
    // affiche le noeud avec deux triangles plein
    
    float d=5;
    zmin=zmin/d;
    zmax=zmax/d;
    
    Pixel phd=divise(n->phd,d);
    Pixel phg=divise(n->phg,d);
    Pixel pbd=divise(n->pbd,d);
    Pixel pbg=divise(n->pbg,d);

    // cout << "test" << n->phd.altitude(zmin,zmax)/d;
    // cout << "test2" << n->phg.altitude(zmin,zmax)/d;
    // cout << "test3" << n->pbd.altitude(zmin,zmax)/d;
    // cout << "test4" << n->pbg.altitude(zmin,zmax)/d;

    glBegin(GL_LINE_STRIP);

        glVertex3d(phd.x,phd.y,n->phd.altitude(zmin,zmax)/d);
        glVertex3d(phg.x,phg.y,n->phg.altitude(zmin,zmax)/d);
        glVertex3d(pbg.x,pbg.y,n->pbg.altitude(zmin,zmax)/d);

        glVertex3d(phd.x,phd.y,n->phd.altitude(zmin,zmax)/d);
        glVertex3d(pbd.x,pbd.y,n->pbd.altitude(zmin,zmax)/d);
        glVertex3d(pbg.x,pbg.y,n->pbg.altitude(zmin,zmax)/d);


    glEnd();
}

void affichevide (Node* n,float zmin,float zmax){
    if (n->isLeaf()){
        SDLNodevide(n, zmin, zmax);
    }
    else{
        affichevide(n->e1, zmin, zmax);
        affichevide(n->e2, zmin, zmax);
        affichevide(n->e3, zmin, zmax);
        affichevide(n->e4, zmin, zmax);
    }

}
void affiche (Node* n,float zmin,float zmax){
    //fonction d'affichage à l'aide du frustum culling
    if (n->isLeaf()){
        SDLNode(n, zmin, zmax);
    }
    else{
        affiche(n->e1, zmin, zmax);
        affiche(n->e2, zmin, zmax);
        affiche(n->e3, zmin, zmax);
        affiche(n->e4, zmin, zmax);
    }

}

void SDLaffiche(FrustumCulling* frust,QuadTree* q,float zmin,float zmax){
    champvision(frust,q);
    for (int i=0;i<frust->cam.nbNodechamp;i++){
        if (frust->cam.champvision[i].isLeaf())
            SDLNode(&frust->cam.champvision[i],zmin,zmax);
    }
}