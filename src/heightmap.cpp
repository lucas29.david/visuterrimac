#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "../include/heightmap.h"

Image* NouvelleImage(int w,int h){
	Image* I = (Image*) malloc(sizeof(Image));
	I->w = w;
	I->h = h;
	I->dat = (Pixel*) calloc(1,w*h*sizeof(Pixel));
	return I;
}

Image* Charger(const char* fichier){
	int i,j; //i: Largeur & j: Hauteur
	Image* I;
	FILE* F = fopen(fichier,"r");
	if (!F){
		return NULL;
	}
	if(fscanf(F,"%d %d",&i,&j)==false){
		cout << "erreur" << endl;
	}
	I = NouvelleImage(i,j);
	printf("%p", I);
	for(i=0;i<I->w*I->h;i++)
	{
		int c;
		if(fscanf(F,"%d",&c)==false){
			cout << "erreur" << endl;
		}
		I->dat[i].valeur = (unsigned char)c;
	}
	fclose(F);
	return I;
}