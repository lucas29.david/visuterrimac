#include "../include/pixel.h"
#include "../include/quadtree.h"
#include <iostream>

using namespace std;

// fonctions qui permettent de faire des tests et de vérifier si le quadtree fonctionne 
void afficheNode(Node* n){
    if (n==nullptr){
        cout<<"pas de données"<<endl;
    }
    else{
        cout<<"pixel haut gauche : ";
        n->phg.affichePixel();
        cout<<"pixel haut droit : ";
        n->phd.affichePixel();
        cout<<"pixel bas gauche : ";
        n->pbg.affichePixel();
        cout<<"pixel bas droit : ";
        n->pbd.affichePixel();
    }
}

void afficheQuadTreerecursive(Node* n,int i) {
    if (n->isLeaf()){
        cout << "il n'y a qu'un noeud :"<<endl;
        afficheNode(n);
    }             
    else{
        cout <<"niveau "<<i<<" :"<<endl;
        cout <<"\tenfant bas gauche :"<<endl;
        cout<<"\t\t";
        if (n->e3->isLeaf()){
            afficheNode(n->e3);
        }
        else{
            afficheNode(n->e3);
            afficheQuadTreerecursive(n->e3,i+1);
        }
        cout <<"\tenfant haut gauche :"<<endl;
        cout<<"\t\t";
        if (n->e1->isLeaf()){
            afficheNode(n->e1);
        }
        else{
            afficheNode(n->e1);
            afficheQuadTreerecursive(n->e1,i+1);
        }
        cout <<"\tenfant haut droit :"<<endl;
        cout<<"\t\t";
        if (n->e2->isLeaf()){
            afficheNode(n->e2);
        }
        else{
            afficheNode(n->e2);
            afficheQuadTreerecursive(n->e2,i+1);
        }

        cout <<"\tenfant bas droit :"<<endl;
        cout<<"\t\t";
        if (n->e4->isLeaf()){
            afficheNode(n->e4);
        }
        else{
            afficheNode(n->e4);
            afficheQuadTreerecursive(n->e4,i+1);
        }
    }
};

void afficheQuadTree (Node* n){

        cout<<"Affichage QuadTree"<<endl;
        afficheQuadTreerecursive(n,1);
    }