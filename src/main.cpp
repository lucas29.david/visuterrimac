#define GL_GLEXT_PROTOTYPES
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <GL/glut.h>
#include <GL/glu.h>

#include "../include/quadtree.h"
#include "../include/geometry.h"
#include "../include/heightmap.h"
#include "../include/camera.h"
#include "../include/frustumculling.h"
#include "../include/affichage.h"

using namespace std;

static const unsigned int WINDOW_WIDTH = 500;
static const unsigned int WINDOW_HEIGHT = 500;

static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

//variables de départ

QuadTree quadtree;
Node node;
int* zmin;
int* zmax;
float theta=80;
float phi=30;
Vect3D poscam=createVect3D(0,0,1);
Vect3D ptvise=pointvise(poscam,theta, phi);
Vect3D haut=up(theta,phi);
FrustumCulling* frustumculling;




//Caméra
void display(void) {   
    glClearColor(0.5,0.5,0.5,0.0);   
    glClear(GL_COLOR_BUFFER_BIT);    
    glMatrixMode(GL_MODELVIEW);  
    cout << poscam.x << endl; 
    cout << poscam.y << endl; 
    cout << poscam.z << endl; 
    cout << ptvise.x << endl; 
    cout << ptvise.y << endl; 
    cout << ptvise.z << endl; 
    cout << haut.x << endl; 
    cout << haut.y << endl; 
    cout << haut.z << endl;
    gluLookAt(poscam.x,poscam.y,poscam.z,ptvise.x,ptvise.y,ptvise.z,haut.x,haut.y,haut.z); 
    affichevide (&node, *zmin,*zmax);
    //SDLaffiche(frustumculling,&quadtree, *zmin, *zmax);
    glFlush();  
} 

int main(int argc, char** argv){
	FILE* F = fopen("image/information.timac","r");
	if (!F){
        cout << "erreur chargement fichier information." << endl;
        return 0;
    }
    char* nom;
    int* longueur; 
    int* largeur;
    // int* zmin;
    // int* zmax;
    int* znear;
    int* zfar;
    int* fov;
    nom = (char*) malloc(sizeof(char)*20);
    longueur = (int*) malloc(sizeof(int));
    largeur = (int*) malloc(sizeof(int));
    zmin = (int*) malloc(sizeof(int));
    zmax = (int*) malloc(sizeof(int));
    znear = (int*) malloc(sizeof(int));
    zfar = (int*) malloc(sizeof(int));
    fov = (int*) malloc(sizeof(int));

	if(fscanf(F,"%s",nom)!=true){
        return 0;
    }
	if(fscanf(F,"%d",longueur)!=true){
        return 0;
    }
	if(fscanf(F,"%d",largeur)!=true){
        return 0;
    }
	if(fscanf(F,"%d",zmin)!=true){
        return 0;
    }
	if(fscanf(F,"%d",zmax)!=true){
        return 0;
    }
	if(fscanf(F,"%d",znear)!=true){
        return 0;
    }
	if(fscanf(F,"%d",zfar)!=true){
        return 0;
    }
	if(fscanf(F,"%d",fov)!=true){
        return 0;
    }

    cout << nom << endl;
    cout << *longueur << endl;
    cout << *largeur << endl;
    cout << *zmin << endl;
    cout << *zmax << endl;
    cout << *znear << endl;
    cout << *zfar << endl;
    cout << *fov << endl;
    // ---------------
    // ## NE FONCTIONNE PAS CAR ERREUR DE SEGMENTATION :
    // ---------------

    // char* nom;
    // nom = (char*) malloc(sizeof(char)*20);

    // char* chemin;
    // chemin = (char*) malloc(sizeof(char)*50);
    // chemin = (char*)"../image/";

	// if(fscanf(F,"%s",nom)!=true){
    //     return 0;
    // }
    // cout << chemin << nom << endl;

    // char* fichier;
    // fichier = (char*) malloc(sizeof(char)*100);
    // cout << "test4.1" << endl;
    // strcat(chemin,nom);
    // cout << chemin << endl;
    // cout << "test5" << endl;
    // cout << fichier;

    // ---------------

    char* fichier;
    fichier = (char*) "image/heightmap.pgm";

    if(Charger(fichier)==NULL){
        cout << "erreur chargement image" << endl;
        return 0;
    }
    Image* image = Charger(fichier);
    cout << image->w << endl;

    Pixel p1;
    p1 = image->dat[0];
    p1.x = -250;
    p1.y = -250;

    Pixel p2;
    p2 = image->dat[499];
    p2.x = 250;
    p2.y = -250;

    Pixel p3;
    p3 = image->dat[500*499];
    p3.x = -250;
    p3.y = 250;
    
    Pixel p4;
    p4 = image->dat[500*500-1];
    p4.x = 250;
    p4.y = 250;

    // for (int i = 0; i < 250000; i++)
    // {
    //     cout << image->dat[i].valeur;
    // }


    node.createNode(p1, p2, p3, p4);
    node.enfant(image->dat, *longueur, *largeur, 0, 0);
    //afficheNode(node.e3);
    //afficheNode(node.e4);
    //afficheQuadTree(&node);

    frustumculling= (FrustumCulling*) malloc(sizeof(FrustumCulling));
    quadtree.initQuadTree(&node);
    frustumculling->init(*zfar,*znear, poscam, *fov);



    //fenêtre  
    glutInit(&argc,argv);  
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);  
    glutInitWindowSize(500,500);  
    glutInitWindowPosition(100, 50);  
    glutCreateWindow("Fenetre OpenGL"); 
    glEnable(GL_DEPTH_TEST);  
    glEnable(GL_COLOR_MATERIAL);  
    glutDisplayFunc(display);  
    glutMainLoop() ;  
    return(0); 

    // cout<<"nombre de noeud : "<<node.countNode()<<endl;

    // bool modeRotation=false;
    // float theta=80;
    // float phi=30;
    // Vect3D poscam=createVect3D(0,0,0);
    // Vect3D ptvise=pointvise(poscam,theta, phi);
    // Vect3D haut=up(theta,phi);

    // /* erreur de segmentation*/
    
    
    // FrustumCulling frustumculling;
    // frustumculling.init(*zfar,*znear, poscam, *fov);


    // fenêtre SDL qui entre en conflit avec glut
    // if(SDL_Init(SDL_INIT_VIDEO) < 0) 
    // {
    //     const char* error = SDL_GetError();
    //     fprintf(
    //         stderr, 
    //         "Erreur lors de l'initialisation de la SDL : %s\n", error);

    //     SDL_Quit();
    //     return EXIT_FAILURE;
    // }
    // SDL_Window* window;
    // {
    //     window = SDL_CreateWindow(
    //     "",
    //     SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    //     WINDOW_WIDTH, WINDOW_HEIGHT,
    //     SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    //     if(NULL == window) 
    //     {
    //         const char* error = SDL_GetError();
    //         fprintf(
    //             stderr,
    //             "Erreur lors de la creation de la fenetre : %s\n", error);

    //         SDL_Quit();
    //         return EXIT_FAILURE;
    //     }
    // }
    // SDL_GLContext context;
    // {
    //         SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    //         SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    //         SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    //     context = SDL_GL_CreateContext(window);
    
    //     if(NULL == context) 
    //     {
    //         const char* error = SDL_GetError();
    //         fprintf(
    //             stderr,
    //             "Erreur lors de la creation du contexte OpenGL : %s\n", error);

    //         SDL_DestroyWindow(window);
    //         SDL_Quit();
    //         return EXIT_FAILURE;
    //     }
    // }
    // int loop = 1;
    // while(loop) 
    // {
        
    //     Uint32 startTime = SDL_GetTicks();
        
    //     
    //     glClear(GL_COLOR_BUFFER_BIT);
    //     glMatrixMode(GL_MODELVIEW);
    //     glLoadIdentity();
    //     SDLNodevide(&node,*zmin,*zmax);

    //     SDLaffiche(&node,*zmin,*zmax);
        

    //     SDL_GL_SwapWindow(window);
        
    //     /* Boucle traitant les evenements */
    //     gluLookAt(poscam.x,poscam.y,poscam.z,ptvise.x,ptvise.y,ptvise.z,haut.x,haut.y,haut.z);



            // ici les évènements à gérer
    //     SDL_Event e;
    //     while(SDL_PollEvent(&e)) 
    //     {
    //         if(e.type == SDL_QUIT) 
    //         {
    //             loop = 0;
    //             break;
    //         }
    //         if (e.type==SDL_KEYDOWN){
    //             //donc là le principe serait que si on apuit sur espace on fait tour,ner la caméra
    //             //donc ça ça marche pas
    //             //peut etre faire 2 modes  comme ça on clique sur espace pour le changer et si on est en mode rotation ou non
    //             //bon ça marche pas je sais pas pourquoi
    //             if (e.key.keysym.sym==SDLK_ESCAPE and modeRotation==false){
    //                 modeRotation=true;
    //             }
    //             if (e.key.keysym.sym==SDLK_ESCAPE and modeRotation==true){
    //                 modeRotation=false;
    //             }
    //             if ( modeRotation==true){    
    //                 if (e.key.keysym.sym==SDLK_UP){
    //                     phi++;
    //                 }
    //                 if (e.key.keysym.sym==SDLK_DOWN){
    //                     phi--;
    //                 }
    //                 if (e.key.keysym.sym==SDLK_RIGHT){
    //                     theta++;
    //                 }
    //                 if (e.key.keysym.sym==SDLK_LEFT){
    //                     theta--;
    //                     cout<<"escape theta="<<theta<<endl;
    //                 }   
    //             }
    //             else{//if ( modeRotation==true){
    //                 if (e.key.keysym.sym==SDLK_UP){
    //                     poscam.y++;
    //                 }
    //                 if (e.key.keysym.sym==SDLK_DOWN){
    //                     poscam.y--;
    //                 }
    //                 if (e.key.keysym.sym==SDLK_RIGHT){
    //                     poscam.x++;
    //                 }
    //                 if (e.key.keysym.sym==SDLK_LEFT){
    //                     poscam.x--;
    //                     cout<<" x="<<poscam.x<<endl;
    //                 }
    //             }
    //              //on modifie les valeurs de gluLookAt
    //             ptvise=pointvise(poscam,theta, phi);
    //             haut=up(theta,phi);
    //         }
    //     }

    //     Uint32 elapsedTime = SDL_GetTicks() - startTime;
    //     if(elapsedTime < FRAMERATE_MILLISECONDS) 
    //     {
    //         SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
    //     }
    // }

    // SDL_GL_DeleteContext(context);
    // SDL_DestroyWindow(window);
    // SDL_Quit();
    
    // return EXIT_SUCCESS;

}

