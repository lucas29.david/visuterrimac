#include "../include/geometry.h"
#include "../include/camera.h"
#include <math.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <GL/gl.h>


//Caméra

Vect3D direction (float theta,float phi){
    return createVect3D(cos(theta)*sin(phi),sin(theta)*sin(phi),cos(phi));
}

Vect3D pointvise (Vect3D position,float theta,float phi){
    Vect3D direct=direction(theta,phi);
    return createVect3D(position.x+direct.x,position.y+direct.y,position.z+direct.z);
}

Vect3D left(float theta){
    return createVect3D(cos(theta+M_PI_2),sin(theta+M_PI_2),0);
}

Vect3D up(float theta,float phi){
    Vect3D direct=direction(theta,phi);
    Vect3D gauche=left(theta);
    return produitvectoriel(direct,gauche);
}




//LOD

Vect3D plusproche(Node n, Vect3D poscam){
    //fonction qui permet d'avoir le point le plus proche de la Caméra 
    //parmis les 4 points coordonnées du noeud
    float d[4];
    Vect3D v[4];
    v[0]=createVect3D(n.pbd.x,n.pbd.y,0.);
    d[0]=distance(v[0],poscam);
    v[1]=createVect3D(n.phd.x,n.phd.y,0.);
    d[1]=distance(v[1],poscam);
    v[2]=createVect3D(n.pbg.x,n.pbg.y,0.);
    d[2]=distance(v[2],poscam);
    v[3]=createVect3D(n.phg.x,n.phg.y,0.);
    d[3]=distance(v[3],poscam);
    int imin=0;
    for (int i=1;i<4;i++){
        if (d[imin]>d[i]){
            imin=i;
        }
    }
    return v[imin];
}