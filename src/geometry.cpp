#include "../include/geometry.h"
#include "../include/quadtree.h"
#include <cmath>

Vect3D createVect3D(float x,float y, float z){
    Vect3D v;
    v.x=x;
    v.y=y;
    v.z=z;
    return v;
}

Vect3D produitvectoriel(Vect3D v1,Vect3D v2){
    Vect3D v;
    v.x=v1.y*v2.z-v1.z*v2.y;
    v.y=v1.x*v2.z-v1.z*v2.x;
    v.z=v1.y*v2.x-v1.x*v2.y;
    return v;
}

float Thales(float a,float b, float c){
    return (c*b)/a;
}

float distance(Vect3D v1,Vect3D v2){
    return sqrt((v2.x-v1.x)*(v2.x-v1.x)+(v2.y-v1.y)*(v2.y-v1.y)+(v2.z-v1.z)*(v2.z-v1.z));
}

