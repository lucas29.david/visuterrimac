
/*//========> Création de la CubeMap

#define GL_TEXTURE_CUBE_MAP_ARB             0x8513
#define GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB  0x8515
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB  0x8516
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB  0x8517
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB  0x8518
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB  0x8519
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB  0x851A
 
// Test de l'extension GL_ARB_texture_cube_map
char* extensions = (char*) glGetString(GL_EXTENSIONS); 

if(strstr(extensions, "GL_ARB_texture_cube_map") != NULL)
{
    // Initialisation de la CubeMap
    
} 
else
{
    // Autre système (six textures 2D par exemple) ou sortie de l'application ou toute autre gestion d'erreur
}

// Liste des faces successives pour la création des textures de CubeMap
GLenum cube_map_target[6] = {
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,
    GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB
};
 
// Chargement des six textures
AUX_RGBImageRec * texture_image[6];
texture_image[0] = LoadBMP( "../image/Skybox Images/bottom.bmp" );
texture_image[1] = LoadBMP( "../image/Skybox Images/top.bmp" );
texture_image[2] = LoadBMP( "../image/Skybox Images/front.bmp" );
texture_image[3] = LoadBMP( "../image/Skybox Images/back.bmp" );
texture_image[4] = LoadBMP( "../image/Skybox Images/left.bmp" );
texture_image[5] = LoadBMP( "../image/Skybox Images/right.bmp" );
 
// Génération d'une texture CubeMap
GLuint cube_map_texture_ID;
glGenTextures(1, &cube_map_texture_ID);
 
// Configuration de la texture
glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, cube_map_texture_ID);
 
for (int i = 0; i < 6; i++)
{
    glTexImage2D(cube_map_target[i], 0, 3, texture_image[i]->sizeX, texture_image[i]->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_image[i]->data);
 
    if (texture_image[i])                
    {
        if (texture_image[i]->data)    
        {
            free(texture_image[i]->data);    
        }
        free(texture_image[i]);    
    }
}
 
glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);

// Taille du cube
float t = 1.0f;
 
// Utilisation de la texture CubeMap
glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, cube_map_texture_ID);
 
// Réglage de l'orientation
glPushMatrix();
glLoadIdentity();
glRotatef( camera_pitch, 1.0f, 0.0f, 0.0f );
glRotatef( camera_yaw, 0.0f, 1.0f, 0.0f );    
 
 
// Rendu de la géométrie
glBegin(GL_TRIANGLE_STRIP);            // X Négatif        
    glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);     
    glTexCoord3f(-t,t,-t); glVertex3f(-t,t,-t);
    glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);
    glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
glEnd();
 
glBegin(GL_TRIANGLE_STRIP);            // X Positif
    glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
    glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
    glTexCoord3f(t,t,-t); glVertex3f(t,t,-t); 
    glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
glEnd();
 
glBegin(GL_TRIANGLE_STRIP);            // Y Négatif    
    glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);
    glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);
    glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
    glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
glEnd();
 
glBegin(GL_TRIANGLE_STRIP);            // Y Positif        
    glTexCoord3f(-t,t,-t); glVertex3f(-t,t,-t);
    glTexCoord3f(t,t,-t); glVertex3f(t,t,-t); 
    glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
    glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
glEnd();
 
glBegin(GL_TRIANGLE_STRIP);            // Z Négatif        
    glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);
    glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
    glTexCoord3f(-t,t,-t); glVertex3f(-t,t,-t);
    glTexCoord3f(t,t,-t); glVertex3f(t,t,-t); 
glEnd();
 
glBegin(GL_TRIANGLE_STRIP);            // Z Positif    
    glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);
    glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
    glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
    glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
glEnd();
 
// Réinitialisation de la matrice ModelView
glPopMatrix();

// Initialisation de la scène
glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    
glLoadIdentity();
glColor3f( 1.0f, 1.0f, 1.0f );
 
// Placement de la caméra    
glTranslatef( 0.0f, 0.0f, -8.0f );
glRotatef( CAMERA_Pitch, 1.0f, 0.0f, 0.0f );
glRotatef( CAMERA_Yaw, 0.0f, 1.0f, 0.0f );
 
// Configuration des états OpenGL
glEnable(GL_DEPTH_TEST);
glEnable(GL_TEXTURE_CUBE_MAP_ARB); 
glDisable(GL_LIGHTING);
 
// Désactivation de l'écriture dans le DepthBuffer
glDepthMask(GL_FALSE);
 
// Rendu de la SkyBox
DrawSkyBox( camera_yaw, camera_pitch );
 
// Réactivation de l'écriture dans le DepthBuffer
glDepthMask(GL_TRUE);
 
// Réinitialisation des états OpenGL
glDisable(GL_TEXTURE_CUBE_MAP_ARB); 
glEnable(GL_LIGHTING);
 
// Rendu de la scène
*/